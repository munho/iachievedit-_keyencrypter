//
// ViewController.swift
// keyencrypter
//
// Copyright © 2016 iAchieved.it.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

import Cocoa

class ViewController: NSViewController, NSTextFieldDelegate {

  @IBOutlet weak var keyTextField: NSTextField!
  @IBOutlet weak var ivTextField: NSTextField!
  @IBOutlet weak var messageTextField: NSTextField!
  @IBOutlet weak var encryptButton: NSButton!
  @IBOutlet weak var encryptedTextScrollView: NSScrollView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.keyTextField.delegate = self
    self.ivTextField.delegate = self
    self.messageTextField.delegate = self
  }

  override var representedObject: AnyObject? {
    didSet {
    // Update the view, if already loaded.
    }
  }
  
  @IBAction func encryptPressed(sender: AnyObject) {
    let key     = keyTextField.stringValue
    let iv      = ivTextField.stringValue
    let message = messageTextField.stringValue
  
    var ivHex:[UInt8] = [UInt8](count:8, repeatedValue:0)

    var ivIdx = 0; var i = 0
    for s in iv.unicodeScalars {
      var v:UInt8
      if (s.value >= 97) {
        v = UInt8(s.value - 87)
      } else {
        v = UInt8(s.value - 48)
      }
      i = i + 1
      if (i % 2 == 0) {
        i = 0
        ivHex[ivIdx] <<= 4
        ivHex[ivIdx] |= v
        ivIdx += 1
      } else {
        ivHex[ivIdx] = v
      }
    }
    
    var encrypted:[UInt8] = [UInt8]()
    if encrypt(key:key, message: message, iv:ivHex, encrypted: &encrypted) {
      let hexstring  = hexString(encrypted)
      let textStorage = self.encryptedTextScrollView.documentView!.textStorage!!
      textStorage.setAttributedString(NSAttributedString(string:hexstring))
    } else {
      let alert = NSAlert()
      alert.addButtonWithTitle("OK")
      alert.messageText = "Error"
      alert.informativeText = "Failed to encrypt for some unknown reason."
      alert.alertStyle = .CriticalAlertStyle
      alert.runModal()
    }
    
  }
  
  func enableEncryptButton() -> Bool {
    var keyLengthIsGood:Bool = false
    var ivLengthIsGood:Bool  = false
    var msgLengthIsGood:Bool = false
    
    if self.keyTextField.stringValue.characters.count > 8 {
      keyLengthIsGood = true
    }
    
    if self.ivTextField.stringValue.characters.count == 16 {
      ivLengthIsGood = true
    }
    
    if self.messageTextField.stringValue.characters.count > 0 {
      msgLengthIsGood = true
    }
    
    return keyLengthIsGood && ivLengthIsGood && msgLengthIsGood // write a blog post
  }
  
  override func controlTextDidChange(obj: NSNotification) {

    let textField = obj.object as! NSTextField
    if textField == keyTextField {
      self.encryptButton.enabled = self.enableEncryptButton()
      return
    }
    if textField == messageTextField {
      self.encryptButton.enabled = self.enableEncryptButton()
      return
    }
    
    let characterSet:Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]
    var string       = textField.stringValue
    var cleaned      = ""
    
  
    for c in string.characters {
      if characterSet.contains(c) {
        cleaned.append(c)
      }
    }
    string = cleaned
    
    if string.characters.count > 16 {
      string = string.substringToIndex(string.startIndex.advancedBy(16))
    }
    textField.stringValue = string
    
    self.encryptButton.enabled = self.enableEncryptButton()
    
    
  }
}

