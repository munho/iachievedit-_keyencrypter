//
// encrypt.swift
//
// Copyright © 2016 iAchieved.it.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//


import Foundation

func encrypt(key key:String, message:String, iv:[UInt8], inout encrypted:[UInt8]) -> Bool {
  
  var context:mbedtls_blowfish_context = mbedtls_blowfish_context()
  let keybits = UInt32(key.characters.count*8)
  
  mbedtls_blowfish_init(&context)
  mbedtls_blowfish_setkey(&context, key, keybits)
  
  var eiv     = iv
  var ivOffset = 0
  encrypted = [UInt8](count:message.characters.count, repeatedValue:0)
  
  if mbedtls_blowfish_crypt_cfb64(&context,
                                  MBEDTLS_BLOWFISH_ENCRYPT,
                                  message.characters.count,
                                  &ivOffset,
                                  &eiv,
                                  message,
                                  &encrypted) != 0 {
    return false
  }
  
  mbedtls_blowfish_free(&context)

  // Test routine
  //decrypt(key: key, iv: iv, message: encrypted)
  
  return true
}

// Test routine, not usable yet
func decrypt(key key:String, iv:[UInt8], message:[UInt8]) {
  
  var context:mbedtls_blowfish_context = mbedtls_blowfish_context()
  let keybits = UInt32(key.characters.count*8)
  
  mbedtls_blowfish_init(&context)
  mbedtls_blowfish_setkey(&context, key, keybits)

  var div = iv
  var iv_off = 0
  var myOutput:[UInt8] = [UInt8](count:message.count, repeatedValue:0)

  mbedtls_blowfish_crypt_cfb64(&context,
                                MBEDTLS_BLOWFISH_DECRYPT,
                                message.count,
                                &iv_off,
                                &div,
                                message,
                                &myOutput)
  
  _ = String(bytes: myOutput, encoding: NSUTF8StringEncoding)
  mbedtls_blowfish_free(&context)


}

func hexString(bytes:[UInt8]) -> String {
  var hexString = ""
  for byte in bytes {
    var byteHex = String(byte, radix:16)
    if byteHex.characters.count < 2 {
      byteHex = "0" + byteHex
    }
    hexString = hexString + byteHex
  }
  return hexString
}